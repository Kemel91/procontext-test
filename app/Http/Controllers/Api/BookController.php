<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\Book\BookAll;
use App\Services\Book\BookShow;
use App\Services\Book\BookUpdate;
use App\Services\Book\BookDelete;
use App\Http\Requests\BookStoreRequest;

class BookController extends Controller
{
    /**
     * Получение списка книг с именем автора
     * @param BookAll $bookAll
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(BookAll $bookAll) {
        $books = $bookAll();
        return response()->json($books);
    }

    /**
     * Получение данных книги по id
     * @param BookShow $bookShow
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(BookShow $bookShow, int $id) {
        $book = $bookShow($id);
        return response() -> json($book);
    }

    /**
     * Редактирование данных книги
     * @param BookStoreRequest $request
     * @param BookUpdate $bookUpdate
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(BookStoreRequest $request,BookUpdate $bookUpdate, int $id) {
        $data = $request -> validated();
        $bookUpdate($id, $data);
        return response()->noContent();
    }

    /**
     * Удаление книги
     * @param BookDelete $bookDelete
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(BookDelete $bookDelete, int $id)
    {
        $bookDelete($id);
        return response()->noContent();
    }
}
