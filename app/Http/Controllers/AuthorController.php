<?php

namespace App\Http\Controllers;

use App\Services\Author\AuthorAll;
use App\Services\Author\AuthorStore;
use App\Services\Author\AuthorDelete;
use App\Services\Author\AuthorShow;
use App\Services\Author\AuthorUpdate;
use App\Http\Requests\AuthorCreate;

class AuthorController extends Controller
{
    public function index(AuthorAll $authorAll)
    {
        $authors = $authorAll();
        return view('author.index', compact('authors'));
    }

    public function create()
    {
        return view('author.create');
    }

    public function store(AuthorCreate $request, AuthorStore $authorStore)
    {
        $data = $request->validated();
        try {
            $author = $authorStore($data);
            return redirect()->route('author.index')->with(['authorSave' => $author]);
        } catch (\Exception $exception) {
            return redirect()->route('author.create')->with(['error' => $exception->getMessage()]);
        }
    }

    public function show(int $id, AuthorShow $authorShow) {
        $author = $authorShow($id);
        return view('author.show', compact('author'));
    }

    public function edit(int $id, AuthorShow $authorShow) {
        $author = $authorShow($id);
        return view('author.edit', compact('author'));
    }


    public function update(int $id, AuthorCreate $request, AuthorUpdate $authorUpdate) {
        $data = $request->only('name','biography');
        if (!$authorUpdate($id, $data)) {
            return redirect()->route('author.edit',$id);
        }
        return redirect()->route('author.index');
    }

    public function destroy(int $id, AuthorDelete $authorDelete)
    {
        $authorDelete($id);
        return redirect()->route('author.index');
    }
}
