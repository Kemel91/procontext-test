<?php
namespace App\Http\Controllers;

use App\Services\Book\BookAll;
use App\Services\Book\BookStore;
use App\Services\Book\BookShow;
use App\Services\Book\BookUpdate;
use App\Services\Book\BookDelete;
use App\Services\Author\AuthorShow;
use App\Http\Requests\BookStoreRequest;
use Illuminate\Pagination\LengthAwarePaginator;

class BookController extends Controller
{
    private const PAGE_SLICE_COUNT = 10;

    private const PAGE_SLICE_DELIMITER = "\r\n";

    /**
     * Display a listing of the resource.
     *
     * @param BookAll $bookAll
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(BookAll $bookAll)
    {
        $books = $bookAll();
        return view('book.index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param int $id
     * @param AuthorShow $authorShow
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(AuthorShow $authorShow, int $id)
    {
        $author = $authorShow($id);
        return view('book.create', compact('author'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BookStoreRequest $request
     * @param BookStore $bookStore
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(BookStoreRequest $request, BookStore $bookStore)
    {
        $data = $request -> validated();
        try {
            $book = $bookStore($data);
            return redirect()->route('book.index')->with(['bookSave' => $book]);
        } catch (\Exception $exception) {
            return redirect()->route('book.create', $data['author_id'])
                             ->with(['error' => $exception->getMessage()])
                             ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param BookShow $bookShow
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(BookShow $bookShow, int $id)
    {
        $book = $bookShow($id);
        $pages = explode(self::PAGE_SLICE_DELIMITER, $book -> text);
        $paginate = new LengthAwarePaginator($pages, count($pages), self::PAGE_SLICE_COUNT);
        $paginate->setPath(route('book.show',$id));
        $current = $paginate->currentPage() * self::PAGE_SLICE_COUNT - self::PAGE_SLICE_COUNT;
        $text = $paginate->slice($current,self::PAGE_SLICE_COUNT)->implode(self::PAGE_SLICE_DELIMITER);
        return view('book.show', compact('book','text','paginate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @param BookShow $bookShow
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(BookShow $bookShow, int $id)
    {
        $book = $bookShow($id);
        return view('book.edit', compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BookStoreRequest $request
     * @param BookUpdate $bookUpdate
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(BookStoreRequest $request, BookUpdate $bookUpdate, $id)
    {
        $data = $request->validated();
        if (!$bookUpdate($id, $data)) {
            return redirect()->route('book.edit',$id);
        }
        return redirect()->route('book.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param BookDelete $bookDelete
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(BookDelete $bookDelete, int $id)
    {
        $bookDelete($id);
        return redirect()->route('book.index');
    }
}
