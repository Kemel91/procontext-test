<?php

namespace App\Services\Author;

use App\Models\Author;

class AuthorAll
{
    public function __invoke()
    {
        return Author::orderBy('id','asc') -> withCount('books') ->paginate(config('app.paginate_count'));
    }
}
