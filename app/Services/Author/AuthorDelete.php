<?php
namespace App\Services\Author;

use App\Models\Author;

class AuthorDelete
{
    public function __invoke(int $id): bool
    {
        return Author::destroy($id);
    }
}
