<?php
namespace App\Services\Author;

use App\Models\Author;

class AuthorShow
{
    public function __invoke(int $id): Author
    {
        return Author::findOrFail($id);
    }
}
