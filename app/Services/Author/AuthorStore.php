<?php
namespace App\Services\Author;

use App\Models\Author;

class AuthorStore
{

    public function __invoke(array $data): Author
    {
        if (Author::where('name',$data['name']) -> first()) {
            throw new \Exception('Данное имя автора уже содержится в базе данных');
        }
        $author = new Author();
        $author -> name = $data['name'];
        $author -> save();
        return $author;
    }
}
