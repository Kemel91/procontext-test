<?php
namespace App\Services\Author;

use App\Models\Author;

class AuthorUpdate
{
    public function __invoke(int $id, array $data)
    {
        $author = Author::findOrFail($id);
        $author->fill($data);
        return $author->save();
    }
}
