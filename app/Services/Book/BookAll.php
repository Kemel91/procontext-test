<?php
namespace App\Services\Book;

use App\Models\Book;

class BookAll
{

    public function __invoke()
    {
        return Book::with('author')->paginate(config('app.paginate_count'));
    }
}
