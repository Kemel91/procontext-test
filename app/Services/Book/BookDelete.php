<?php
namespace App\Services\Book;

use App\Models\Book;

class BookDelete
{
    public function __invoke(int $id): bool
    {
        return Book::destroy($id);
    }
}
