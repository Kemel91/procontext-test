<?php
namespace App\Services\Book;

use App\Models\Book;

class BookShow
{
    public function __invoke(int $id): Book
    {
        return Book::findOrFail($id);
    }
}
