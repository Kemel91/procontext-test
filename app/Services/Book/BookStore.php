<?php
namespace App\Services\Book;

use App\Models\Book;

class BookStore
{
    public function __invoke(array $data): Book
    {
        if (Book::where('title',$data['title']) -> where('author_id',$data['author_id']) -> first()) {
            throw new \Exception('Данная книга уже содержится в базе данных');
        }
        $book = new Book();
        $book -> title = $data['title'];
        $book -> year = $data['year'];
        $book -> author_id = $data['author_id'];
        $book -> text = $data['text'];
        $book -> save();
        return $book;
    }
}
