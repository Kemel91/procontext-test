<?php
namespace App\Services\Book;

use App\Models\Book;

class BookUpdate
{
    public function __invoke(int $id, array $data)
    {
        $book = Book::findOrFail($id);
        $book->fill($data);
        return $book->save();
    }
}
