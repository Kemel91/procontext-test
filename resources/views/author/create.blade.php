@extends('layout')
@section('content')
    <div class="container fluid">
        <div class="row">
            <div class="col-md-auto">
                @if (session('error'))
                    <div class="alert alert-warning" role="alert">
                        {{ session('error') }}
                    </div>
                @endif
                <form action="{{route('author.store')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="authorName">Имя автора</label>
                        <input type="text" class="form-control" name="name" id="authorName" placeholder="Имя Фамилия">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn-primary">Создать</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
