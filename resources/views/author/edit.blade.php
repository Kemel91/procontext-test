@extends('layout')
@section('content')
    <div class="container fluid">
        <div class="row">
            <div class="col-md-auto">
                @if (session('error'))
                    <div class="alert alert-warning" role="alert">
                        {{ session('error') }}
                    </div>
                @endif
                <form action="{{route('author.update',$author -> id)}}" method="post">
                    @method('PUT')
                    @csrf
                    <div class="form-group">
                        <label for="authorName">Имя автора</label>
                        <input type="text" class="form-control" name="name" id="authorName" value="{{$author -> name}}">
                    </div>
                    <div class="form-group">
                        <label for="authorBio">Биография</label>
                        <textarea class="form-control" id="authorBio" name="biography" rows="3">
                            {{ $author -> biography }}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn-primary">Изменить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
