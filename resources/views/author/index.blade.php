@extends('layout')
@section('content')
    <div class="container fluid">
        <div class="row">
            <div class="col-10">
                @if(!$authors -> isEmpty())
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Имя</th>
                            <th scope="col">Кол-во книг</th>
                            <th scope="col">Добавлен</th>
                            <th scope="col">Изменить</th>
                            <th scope="col">Удалить</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($authors as $author)
                            <tr>
                                <th scope="row">{{ $author -> id }}</th>
                                <td><a href="{{ route('author.show', $author -> id) }}">{{ $author -> name }}</a></td>
                                <td>{{ $author -> books_count }}</td>
                                <td>{{ $author -> created_at }}</td>
                                <td>
                                    <a href="{{ route('author.edit', $author->id) }}">
                                        <img src="{{asset('images/edit.png')}}">
                                    </a>
                                </td>
                                <td>
                                    <form method="post" action="{{ route('author.destroy', $author->id) }}">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-link">
                                            <img src="{{asset('images/delete.png')}}">
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">{{ $authors -> links() }}</div>
                @else
                    <div class="alert alert-primary" role="alert">Данные отсутсвуют</div>
                @endif
            </div>
            <div class="col-2">
                <div class="form-group">
                <a class="btn btn-primary" href="{{route('author.create')}}">Добавить автора</a>
                </div>
                @if (session('authorSave'))
                    <div class="alert alert-success" role="alert">
                        Автор добавлен
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
