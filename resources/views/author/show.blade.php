@extends('layout')
@section('content')
    <div class="container fluid">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{{ $author -> name }}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Биография</h6>
                        <p class="card-text">{{ $author -> biography }}</p>
                        <a href="{{ route('book.create', $author -> id) }}" class="card-link">Добавить книгу</a>
                    </div>
                </div>
    </div>
@endsection
