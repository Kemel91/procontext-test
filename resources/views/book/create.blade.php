@extends('layout')
@section('content')
    <div class="container fluid">
        <div class="row">
            <div class="col-md-auto">
                @if (session('error'))
                    <div class="alert alert-warning" role="alert">
                        {{ session('error') }}
                    </div>
                @endif
                <div class="card">
                    <div class="card-body">
                        <p class="card-text">Добавить книгу автору {{ $author -> name }}</p>
                    </div>
                </div>
                <form action="{{route('book.store')}}" method="post">
                    @csrf
                    <input type="hidden" name="author_id" value="{{ $author -> id }}">
                    <div class="form-group">
                        <label for="bookTitle">Название</label>
                        <input type="text" class="form-control" name="title" id="bookTitle" placeholder="Война и Мир">
                    </div>
                    <div class="form-group">
                        <label for="bookText">Текст</label>
                        <textarea class="form-control" id="bookText" name="text">{{ old('text') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="bookYear">Год издания</label>
                        <input type="number" class="form-control" name="year" id="bookYear" placeholder="2010"
                               min="1800" value="2010">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn-primary">Добавить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
