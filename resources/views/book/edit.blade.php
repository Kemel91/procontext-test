@extends('layout')
@section('content')
    <div class="container fluid">
        <div class="row">
            <div class="col-md-auto">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-warning" role="alert">
                        {{ session('error') }}
                    </div>
                @endif
                <form action="{{route('book.update',$book -> id)}}" method="post">
                    @method('PUT')
                    @csrf
                    <input type="hidden" name="author_id" value="{{ $book -> author_id }}">
                    <div class="form-group">
                        <label for="bookTitle">Название</label>
                        <input type="text" class="form-control" name="title" id="bookTitle" value="{{ $book -> title }}">
                    </div>
                    <div class="form-group">
                        <label for="bookText">Текст</label>
                        <textarea class="form-control" id="bookText" name="text">{{ $book -> text }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="bookYear">Год издания</label>
                        <input type="number" class="form-control" name="year" id="bookYear" value="{{ $book -> year }}" min="1800">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn-primary">Изменить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
