@extends('layout')
@section('content')
    <div class="container fluid">
        <div class="row">
            <div class="col-12">
                @if(!$books -> isEmpty())
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Название</th>
                            <th scope="col">Автор</th>
                            <th scope="col">Год</th>
                            <th scope="col">Изменить</th>
                            <th scope="col">Удалить</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($books as $book)
                            <tr>
                                <th scope="row">{{ $book -> id }}</th>
                                <td><a href="{{ route('book.show', $book -> id) }}">{{ $book -> title }}</a></td>
                                <td>
                                    <a href="{{ route('author.show', $book -> author -> id) }}">
                                        {{ $book -> author -> name }}
                                    </a>
                                </td>
                                <td>{{ $book -> year }}</td>
                                <td>
                                    <a href="{{ route('book.edit', $book->id) }}">
                                        <img src="{{asset('images/edit.png')}}">
                                    </a>
                                </td>
                                <td>
                                    <form method="post" action="{{ route('book.destroy', $book->id) }}">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-link">
                                            <img src="{{asset('images/delete.png')}}">
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">{{ $books -> links() }}</div>
                @else
                    <div class="alert alert-primary" role="alert">Данные отсутсвуют</div>
                @endif
            </div>
        </div>
    </div>
@endsection
