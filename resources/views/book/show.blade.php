@extends('layout')
@section('content')
    <div class="container fluid">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-center">{{ $book -> title }}</h5>
                <h6 class="card-subtitle mb-2 text-muted">{{ $book -> year }}</h6>
                <p class="card-text" style="white-space: pre-line;">{{ $text }}</p>
                <p class="text-center">{{ $paginate -> links() }}</p>
            </div>
        </div>
    </div>
@endsection
